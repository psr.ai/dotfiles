SECONDS=0
export SUDO_EDITOR=nvim

export ZSH="$HOME/.oh-my-zsh"
ZSH_THEME="powerlevel10k/powerlevel10k"
plugins=(git)
source $ZSH/oh-my-zsh.sh

. ~/GitHub/rupa/z/z.sh

# for .config bare repo
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'

export GPG_TTY=$(tty)

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

if [ `uname` = "Darwin" ]; then
	export PATH=/opt/homebrew/bin:$PATH
	PATH=$(pyenv root)/shims:$PATH
	export NVM_DIR="$HOME/.nvm"
	[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
	[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
	
	export PATH="$HOME/softwares/texlab/target/release:$PATH"
	export PATH="/opt/homebrew/opt/openjdk/bin:$PATH"
	eval "$(pyenv init --path)"
elif [ `uname` = "Linux" ]; then
	[ -z "$NVM_DIR" ] && export NVM_DIR="$HOME/.nvm"
	if [ -d /usr/share/nvm ]; then
		source /usr/share/nvm/nvm.sh
		source /usr/share/nvm/bash_completion
		source /usr/share/nvm/install-nvm-exec
	else
		source $NVM_DIR/nvm.sh
		source $NVM_DIR/bash_completion
	fi

	export PYENV_ROOT="$HOME/.pyenv"
	[[ -d $PYENV_ROOT/bin ]] && export PATH="$PYENV_ROOT/bin:$PATH"
	eval "$(pyenv init -)"
	PATH="$PATH:$HOME/usr/bin"
fi

# The next line updates PATH for the Google Cloud SDK.
if [ -f "$HOME/google-cloud-sdk/path.zsh.inc" ]; then . "$HOME/google-cloud-sdk/path.zsh.inc"; fi

# The next line enables shell command completion for gcloud.
if [ -f "$HOME/google-cloud-sdk/completion.zsh.inc" ]; then . "$HOME/google-cloud-sdk/completion.zsh.inc"; fi


alias k=kubectl
k-login() {
	gcloud container clusters get-credentials $1 --region=$2
}



neofetch --backend off
duration=$SECONDS
echo "$((duration / 60)) minutes and $((duration % 60)) seconds elapsed."
