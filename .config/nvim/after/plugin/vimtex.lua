-- Enable filetype detection, plugins, and indentation
vim.cmd('filetype plugin indent on')

-- Enable syntax highlighting
vim.cmd('syntax enable')


-- Configure the compiler for VimTeX
--vim.g.vimtex_compiler_latexmk_engines = {
--  _ = '-xelatex',
--  xelatex = '-xelatex',
--  lualatex = '-lualatex',
--}

vim.g.vimtex_compiler_latexmk = {
  executable = 'latexmk',
  options = {
    '-xelatex',  -- Specifies the use of XeLaTeX
    '-file-line-error',
    '-synctex=1',
    '-interaction=nonstopmode',
  },
}

-- Configure the viewer method for VimTeX
vim.g.vimtex_view_method = 'skim'
vim.g.vimtex_view_skim_sync = 1
vim.g.vimtex_view_skim_activate = 1

vim.g.maplocalleader = ','
