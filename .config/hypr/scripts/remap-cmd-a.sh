app_type=$(hyprctl activewindow | awk -F': ' '/^[\t]*class: /{print $2}')
pid=$(hyprctl activewindow | awk -F': ' '/^[\t]*pid: /{print $2}')

if [ "$app_type" == "Google-chrome" ]; then
	ydotool key -d 100 97:1 30:1 30:0 97:0
else
	#ydotool key 56:1 20:1 20:0 56:0
	hyprctl dispatch pass pid:$pid
fi
